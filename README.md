# sass project



## Getting started
1. descarge el proyecto del repositorio
2. dentro de la raiz del proyecto abra una terminal y ejecute el instalador `npm install`
3. **compilar una unica vez** el codigo sass, para generar una compilacion de codigo SASS a CSS use `npm run build`, esto generara un archivo css en "/css/main.css"
4. **compilar observando cambios**, el codigo funcionara como en el paso 3 pero al realizar algun cambio en el SASS este volvera a compilar todo nuevamente para ejecutar este modo escriba `npm run watch` (este modo consume mas recursos por lo que uselo con criterio).

## Add your files


## Description
Un ejemplo de como compilar codigo SASS en una aplicacion web simple.
